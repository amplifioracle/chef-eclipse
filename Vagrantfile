# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  # All Vagrant configuration is done here. The most common configuration
  # options are documented and commented below. For a complete reference,
  # please see the online documentation at vagrantup.com.

  #config.vm.hostname = "eclipse-berkshelf"

  # Every Vagrant virtual environment requires a box to build off of.
  config.vm.box = "amplifi/oel65-highswap"

  # Assign this VM to a host-only network IP, allowing you to access it
  # via the IP. Host-only networks can talk to the host machine as well as
  # any other machines on the same network, but cannot be accessed (through this
  # network interface) by any external networks.

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.

  # config.vm.network :public_network

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  config.vm.provider :virtualbox do |vb|
    # Don't boot with headless mode
    vb.gui = true
  end
  #
  # View the documentation for the provider you're using for more
  # information on available options.
  
  # Install Chef (if needed by the base box)
  config.vm.provision "shell", inline: "hash chef-solo 2>/dev/null; if [ $? -eq 1 ]; then curl -sL https://www.opscode.com/chef/install.sh | sudo bash 1>/dev/null; fi"

  # The path to the Berksfile to use with Vagrant Berkshelf
  # config.berkshelf.berksfile_path = "./Berksfile"

  # Enabling the Berkshelf plugin. To enable this globally, add this configuration
  # option to your ~/.vagrant.d/Vagrantfile file
  config.berkshelf.enabled = true

  # An array of symbols representing groups of cookbook described in the Vagrantfile
  # to exclusively install and copy to Vagrant's shelf.
  # config.berkshelf.only = []

  # An array of symbols representing groups of cookbook described in the Vagrantfile
  # to skip installing and copying to Vagrant's shelf.
  # config.berkshelf.except = []

  config.vm.provision :chef_solo do |chef|
    chef.json = {
      "eclipse" => {
        "arch" => "x86_64",
        "version" => "luna",
        "url" => "http://dfc855070b5246c89519-53bcef49f1fd19f25440c2e18a31805d.r89.cf1.rackcdn.com/eclipse-jee-luna-R-linux-gtk-x86_64.tar.gz",
        "plugins" => [
          # gradle integration
          {"http://dist.springsource.com/release/TOOLS/gradle" => "org.springsource.ide.eclipse.gradle.feature.feature.group"},
          # groovy integration
          {"http://dist.springsource.org/milestone/GRECLIPSE/e4.4/" => "org.codehaus.groovy.eclipse.feature.feature.group"}
        ],
        "shortcut_users" => ['vagrant']
      }
    }

    chef.run_list = [
        # "recipe[eclipse::default]",
        "recipe[eclipse::desktop_shortcut]"
    ]
  end
end
