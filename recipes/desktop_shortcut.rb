users = node['eclipse']['shortcut_users']

if !users.empty?
  # make sure its an array
  if !users.kind_of?(Array)
    users = [users]
  end

  # install a dekstop shortcut for each user
  users.each do |user|
    desktop_path = "/home/#{user}/Desktop"
    shortcut_path = "#{desktop_path}/Eclipse.desktop"
    directory desktop_path do
      owner user
      group user
      mode '0755'
      action :create
      recursive true
    end

    template shortcut_path do
      source "Eclipse.desktop.erb"
      action :create
      owner user
      group user
      mode "0755"
    end
  end
end
